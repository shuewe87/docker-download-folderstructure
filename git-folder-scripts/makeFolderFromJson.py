import subprocess, json, sys, os


def createGitConfig(element):
  if "username" in element:
    subprocess.call(["git","config","--global","user.name",element["username"].encode("utf-8")])
  if "usermail" in element:
    subprocess.call(["git","config","--global","user.email",element["usermail"].encode("utf-8")])


def createNormalFolder(element):
  if not os.path.exists(element["parent"]+element["name"]):
    subprocess.call(["mkdir",element["name"]])

def createGitFolder(element):
  if not os.path.exists(element["parent"]+element["name"]):
    subprocess.call(["git","clone",element["url"]])
  os.chdir(element["name"])
  subprocess.call(["git","checkout",element["branch"]])
  if "post" in element:
   if element["post"]=="pip":
     if "pip-mode" in element:
      subprocess.call(["pip","install",element["pip-mode"],"."])
     else:
      subprocess.call(["pip","install","."])

def createTarDownloadFolder(element):
  if not os.path.exists(element["parent"]+element["name"]):
    if not os.path.isfile(element["name"]+".tar"):
      subprocess.call(["wget",element["url"]])
    subprocess.call(["tar","-xvf",element["name"]+".tar"])


def main(data): 
  if "git-config" in data:
    createGitConfig(data["git-config"])
  for element in data["elements"]:
    os.chdir(element["parent"])
    if element["foldertype"] == "normal":
     createNormalFolder(element)
    if element["foldertype"] == "git":
     createGitFolder(element)
    if element["foldertype"] == "tar-download":
     print("tar")
     createTarDownloadFolder(element)
    


if __name__ == "__main__":
   if len(sys.argv)==1:
     print("No source given")
     sys.exit()
   else:
    iniPath=os.getcwd()
    for path in sys.argv[1:]:
     with open(path,encoding="utf-8") as f:
      data=json.load(f) 
     main(data)
    os.chdir(iniPath)

