#!/bin/bash

setOptions(){
echo ""
echo "Start reading options for git folderstructure image"
ADDITIONS=()
FILES=()
PSCRIPT=()
 while true; do
        case "${1}" in
            --add-definition )
                FILES+=("$2")
                echo "JSON definition added: $2."
                shift 2 ;;
            --add-post-script )
                PSCRIPT+=("$2")
                echo "Post script added: $2."
                shift 2 ;;
            "")
                break ;;
            * )
                ADDITIONS+=("$1")
                TO_EXEC=${ADDITIONS[@]}
                shift ;;
        esac
 done

echo "Path to execute: $TO_EXEC"
echo "reading options finished"
echo ""

}

##
#Main script
##

setOptions "$@"

for file in "${FILES[@]}"  
do  
    echo "Create folder structure from $file"
    python /git-folder-scripts/makeFolderFromJson.py $file
done  

for file in "${PSCRIPT[@]}"
do
   echo "Post script $file executed.."
   bash $file
done

exec ${TO_EXEC[@]}
